var express = require('express');
var request = require('request');
var rp = require('request-promise');
var async = require('async');
var bodyparser = require('body-parser');
var connection = require('./dbConnection');
var jwt = require('jsonwebtoken'); 
var app = express();
var config = require('./config');
var user = {};

// =======================
// configuration =========
// =======================
var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
var server_address = '0.0.0.0';
app.set('superSecret', config.secret); // secret variable

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

connection.init();


var apiRoutes = express.Router(); 

apiRoutes.post('/authenticate', function(req, res) {
    print("POST /authenticate ");
    connection.acquire(function(err, con) {
        con.query("select idUtente from utente where email = '"+req.body.email+"' and password = '"+req.body.password+"' limit 1", function(err, result) {
            con.release();

            if (err) {
                console.log("ERRORE\n"+JSON.stringify(err));
                res.send({status: 1, message: err});
            } else if( result.length < 1 ) {
                res.send({status: 2, message: 'No users found'})
            } else {
            	user.id = result[0].idUtente;
                user.email = req.body.email;

                var token = jwt.sign(user, app.get('superSecret'), {
                    expiresIn : 60*60*24 // expires in 24 hours
                });

                // return the information including token as JSON
                res.json({
                    status: 0,
                    success: true,
                    message: 'Success',
                    token: token
                });                
            }
        });
    });
});

apiRoutes.post('/register', function(req, res){
    print("POST /register "+JSON.stringify(req.body.email));
    var email = req.body.email;
    var password = req.body.password;

    connection.acquire(function(err, con) {    	
        con.query("INSERT INTO utente (email, password) VALUES ('"+req.body.email+"', '"+req.body.password+"')", function(err, result) {
            con.release();
            if (err) {
                res.send({status: 1, message: err});
            } else {
                res.send({status: 0, message: 'Success'});
            }
        });
    }); 
});

//Middleware Token verifier
apiRoutes.use(function(req, res, next) {
  	// check header or url parameters or post parameters for token
  	var token = req.headers['x-access-token'];

  	// decode token
  	if (token) {
	    // verifies secret and checks exp
	    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
			if (err) {
				return res.json({ success: false, message: 'Failed to authenticate token.' });    
			} else {
				// if everything is good, save to request for use in other routes
				req.id = decoded.id;
				next();
			}
	    });
  	} else {
	    // if there is no token
	    // return an error
	    return res.status(403).send({ 
	        success: false, 
	        message: 'No token provided.' 
	    });
  	}
});

apiRoutes.post('/plants', function(req, res){
	print("POST /api/plants");			
	function buildQuery(plants){
		return new Promise(function(fulfill, reject){
			var plantsId = req.body.plantsId;
			var dim = req.body.dim;

			if(plantsId.length > 0){
				var query = "SELECT T.*, SUM(cons) as cons FROM("
				plantsId.forEach(function(plantId, index, arr) {
					if(index === arr.length-1){
						query = query + '  SELECT pianta.idPianta, pianta.nome, cons'
	        							+' FROM pianta , relazione'
	        							+' WHERE idP1 = '+plantId
								        +' AND idP2 = pianta.idPianta'
								        +' AND pianta.spaceSprout <= '+dim
								        +' UNION ALL'
								        +' SELECT pianta.idPianta, pianta.nome, 0 as cons'
								        +' FROM pianta'
								        +' WHERE NOT EXISTS ('
								            +' SELECT *'
								            +' FROM relazione'
								            +' WHERE idP1 = '+plantId
								            +' AND idP2 = pianta.idPianta'
								        +' )'
								        +' AND pianta.spaceSprout <= '+dim
								        +' ) as T'
										+' GROUP BY idPianta'
										+' ORDER BY cons DESC';

						fulfill(query);
					}
					else{
						query = query + '  SELECT pianta.idPianta, pianta.nome, cons'
	        							+' FROM pianta , relazione'
	        							+' WHERE idP1 = '+plantId
								        +' AND idP2 = pianta.idPianta'
								        +' AND pianta.spaceSprout <= '+dim
								        +' UNION ALL'
								        +' SELECT pianta.idPianta, pianta.nome, 0 as cons'
								        +' FROM pianta'
								        +' WHERE NOT EXISTS ('
								            +' SELECT *'
								            +' FROM relazione'
								            +' WHERE idP1 = '+plantId
								            +' AND idP2 = pianta.idPianta'
								        +' )'
								        +' AND pianta.spaceSprout <= '+dim
								        +' UNION ALL'
					}
				});
			} else {
				fulfill("SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE spaceSprout <= "+dim+" ORDER BY nome ASC");
			}
		})
	}

	function getConsociations(){
		return new Promise(function(fulfill, reject){
			buildQuery().then(function(query){

				connection.acquire(function(err, con){
					con.query(query, function(err, result){
						if(err){
							console.log("SQLERROR: "+err);
							reject(err);
						} else {
							fulfill(result);
						}
					});
				});
			});
		});
	}

	getConsociations()
	.then(function(result){
		res.json(result);
	})
	.catch(function(err) {
		console.log("Rejected Error: "+err);
        res.json(err);
    });
});

apiRoutes.get('/plant/:id', function(req, res){
	print("GET /api/plant/"+req.params.id);
	connection.acquire(function(err, con){
		con.query("select * from pianta where idPianta = '"+req.params.id+"' LIMIT 1", function(err, result){
			if(err){
				res.json(err);
			} else {
				res.json(result[0]);
			}
		})
	});
});

apiRoutes.get('/cultivations', function(req, res){
	print("GET /api/cultivations");
	var userId = req.id;

	function getCultivation(){
		return new Promise(function(fulfill, reject){
			connection.acquire(function(err, con){
				con.query("select * from coltivazione where idUtente = '"+userId+"' AND deleted = 0 ORDER BY dataInizio DESC", function(err, result){
					con.release();
					if(err){
						reject(err);
					} else if (result.length < 1){
						reject("No cultivations found");
					} else {
						fulfill(result);
					}
				});
			});
		});
	};

	function getCultivationData(){
		return new Promise(function(fulfill, reject){
			getCultivation().then(function(result){
				result.forEach(function(cult, index, arr){
					cult.piante = [];
					connection.acquire(function(err, con){
						con.query("SELECT * FROM pianta , piantecoltivazione WHERE piantecoltivazione.idColtivazione = '"+cult.id+"' AND pianta.idPianta = piantecoltivazione.idPianta", function(err, piante){
							con.release();
							if(err){
								reject(err);
							} else {
								cult.piante = piante;
								if(index === arr.length-1)
									fulfill(result);
							}
						});
					})
				});
			}, reject);
		})
	};

	function getWeatherData(){
		return new Promise(function(fulfill, reject){
			getCultivationData().then(function(result){

				result.forEach(function(cult, index, arr) {
	  				var options = {
	  				    uri: 'http://api.openweathermap.org/data/2.5/weather?lat='+cult.latitude+'&lon='+cult.longitude+'&units=metric&APPID=298737a111eadf9a873a83ddb8821205',
	  				    method: 'GET',
	  				    headers: {
	  				        'Content-Type': 'application/json'
	  				    },
	  				    json: true // Automatically parses the JSON string in the response 
	  				};
		  			rp(options)
	  			    .then(function (body) {
	  			    	
	  	    			var content = body;
	  	    	  		var weather = {};

	  	    	  		weather.description = content.weather[0].description;
	  	    	  		weather.icon = content.weather[0].icon;
	  	    	  		weather.temp = content.main.temp;

	  	    	    	cult.weather = weather;
	  	    	    	cult.cityName = content.name;

	  	    	    	if(index === arr.length-1)
	  	    	    		fulfill(result);
	  			    })
	  			    .catch(function (err) {
	  			        reject(err);
	  			    });
				});
			}, reject);
		})
	}

	getWeatherData()
	.then(function(result){
		res.json({
			status: 1,
			body: result
		});
	})
	.catch(function(err) {
		console.log("ERR: "+err);
        res.json({
        	status : 0,
        	message: err
        });
    });
});

apiRoutes.post('/cultivations', function(req, res){
	print("POST /api/cultivations");
    
	var userId = req.id;
	var nome = req.body.nome;
	var tipo = req.body.tipo;
	var days = req.body.days;
	var lat = req.body.latitude;
	var lng = req.body.longitude;
	var piante = req.body.piante;

	connection.acquire(function(err, con){
		con.query("INSERT INTO coltivazione (idUtente, nome, tipo, latitude, longitude, dataInizio, dataFine) VALUES ('"+userId+"', '"+nome+"', '"+tipo+"', '"+lat+"', '"+lng+"', NOW(), NOW() + INTERVAL "+days+" DAY);", function(err, result){
			con.release();
			if(err){
				console.log("SQLERROR: "+err);
			    res.send({status: 1, message: err});
			} else {
				var insertId = result.insertId;
				piante.forEach(function(idPianta, index, arr){
					connection.acquire(function(err, con){
						con.query("INSERT INTO piantecoltivazione (idColtivazione, idPianta) VALUES ('"+insertId+"', '"+idPianta+"')", function(err, result2){
							if(err){
								console.log("SQLERROR: "+err);
							    res.send({status: 1, message: err});
							} else {
								if(index === arr.length-1){
									res.json({status: 2, message: "Coltivazione aggiunta con successo"});
								}
							}
						});
					});
				});
			}
		});
	});
});

apiRoutes.post('/cultivations/hide', function(req, res){
	print("POST /api/cultivations/hide ");
	var idCult = req.body.id;
	var query = "UPDATE coltivazione SET deleted = 1 WHERE id = "+idCult+" AND idUtente = "+req.id;
	connection.acquire(function(err, con){
		con.query(query, function(err, result){
			con.release();
			if(err){
				console.log("SQL ERROR: "+err);
				res.json({success: false});
			} else {
				res.json({success: true});
			}
		});
	});		
});

apiRoutes.post('/cultivations/complete', function(req, res){
	print("POST /api/cultivations/hide");
	var idCult = req.body.id;
	var query = "UPDATE coltivazione SET dataFine = NOW() WHERE id = "+idCult+" AND idUtente = "+req.id;
	connection.acquire(function(err, con){
		con.query(query, function(err, result){
			con.release();
			if(err){
				console.log("SQL ERROR: "+err);
				res.json({success: false});
			} else {
				res.json({success: true});
			}
		});
	});
});

apiRoutes.delete('/cultivations/plants/delete', function(req, res, next) {
	print("DELETE /api/cultivations/plant/delete");
	var idPianta = req.body.idPianta;
	var idColt = req.body.idColt;
	var query = "delete from piantecoltivazione where idPianta = '"+idPianta+"' AND idColtivazione = "+idColt+" LIMIT 1";
	connection.acquire(function(err, con){
		con.query(query, function(err, result){
			con.release();
			if(err){
				res.json({success: false});
			} else {
				res.json({success: true});
			}
		});
	});
});

apiRoutes.get('/cultivations/weather', function(req, res){
	print("GET /api/cultivations/weather");
	var userId = req.id;

	function getCultivation(){
		return new Promise(function(fulfill, reject){
			connection.acquire(function(err, con){
				con.query("select id, nome, latitude, longitude from coltivazione where idUtente = '"+userId+"' AND deleted = 0 AND dataFine > NOW() ORDER BY dataInizio DESC", function(err, result){
					con.release();
					if(err){
						reject(err);
					} else if (result.length < 1){
						reject("No cultivations found");
					} else {
						fulfill(result);
					}
				});
			});
		});
	};

	function getCultivationData(){
		return new Promise(function(fulfill, reject){
			getCultivation().then(function(result){
				result.forEach(function(cult, index, arr){
					cult.piante = [];
					connection.acquire(function(err, con){
						con.query("SELECT piantecoltivazione.id, pianta.nome, pianta.temp_ger_min, pianta.temp_ger_max, pianta.temp_cre_min, pianta.temp_cre_max FROM pianta, piantecoltivazione WHERE piantecoltivazione.idColtivazione = '"+cult.id+"' AND pianta.idPianta = piantecoltivazione.idPianta", function(err, piante){
							con.release();
							if(err){
								reject(err);
							} else {
								cult.piante = piante;
								if(index === arr.length-1)
									fulfill(result);
							}
						});
					})
				});
			}, reject);
		})
	};

	function getWeatherData(){
		return new Promise(function(fulfill, reject){
			getCultivationData().then(function(result){

				result.forEach(function(cult, index, arr) {
				  	
	  				var options = {
	  				    uri: 'http://api.openweathermap.org/data/2.5/weather?lat='+cult.latitude+'&lon='+cult.longitude+'&units=metric&APPID=298737a111eadf9a873a83ddb8821205',
	  				    method: 'GET',
	  				    headers: {
	  				        'Content-Type': 'application/json'
	  				    },
	  				    json: true // Automatically parses the JSON string in the response 
	  				};
		  			rp(options)
	  			    .then(function (body) {
	  	    			var content = body;
	  	    	    	cult.weather = content.main;

	  	    	    	//Current Temp
	  	    	    	if(index === arr.length-1)
	  	    	    		fulfill(result);
	  			    })
	  			    .catch(function (err) {
	  			        reject(err);
	  			    });

				});
			}, reject);
		})
	}

	function getNotificationMessages(){
		return new Promise(function(fulfill, reject){
			var notifiche = [];
			getWeatherData().then(function(result){
				result.forEach(function(colt, index1, arr1){
					colt.piante.forEach(function(pianta, index2, arr2){
						var notifica = {};

						//console.log(JSON.stringify(pianta));
						notifica.idColtivazione = colt.id;
						notifica.coltivazione = colt.nome;
						notifica.pianta = pianta.nome;
						notifica.id = pianta.id;
						notifica.message = createMessage(pianta, colt.weather);
						notifiche.push(notifica);

						if((index1 === arr1.length-1) && (index2 === arr2.length-1)){
							fulfill(notifiche);
						}
					});
				});
			},reject);
		});
	}

	function createMessage(pianta, weather){
		var ideale = (pianta.temp_cre_max+pianta.temp_cre_min)/2;

		var cold_sentences = [
			"Che freddo che fa!",
			"Oggi ho freddo..",
			"Si congela oggi!",
			"Brrr..."
		];

		var warm_sentences = [
			"Che afa che fa!",
			"Oggi fa caldo...",
			"Che caldo!",
			"Aiuto! Qua si bolle!"
		];

		//Se compresa nei parametri
		if( weather.temp >= pianta.temp_cre_min && weather.temp <= pianta.temp_cre_max ){
			return null;
		}
		//Se minore della minima 
		else if( weather.temp < pianta.temp_cre_min &&  weather.temp < pianta.temp_cre_max ){
			return cold_sentences[Math.floor(Math.random()*cold_sentences.length)]+" La mia temperatura ideale è di "+ideale+"° circa";
		} 
		//Se maggiore della massima
		else if( weather.temp > pianta.temp_cre_max && weather.temp > pianta.temp_cre_min ){
			return warm_sentences[Math.floor(Math.random()*warm_sentences.length)]+" La mia temperatura ideale è di "+ideale+"° circa";
		}
	}

	getNotificationMessages()
	.then(function(result){
		var response = [];
		result.forEach(function(notifica, index, arr){
			var query = ""
			if(notifica.message == null){
				query = "UPDATE piantecoltivazione SET message = NULL WHERE id = "+notifica.id
			} else {
				query = "UPDATE piantecoltivazione SET message = '"+notifica.message+"' WHERE id = "+notifica.id
			}

			connection.acquire(function(err, con){
				con.query(query, function(err, piante){
					con.release();
					if(err){
						console.log("SQL ERROR "+err);
						res.json({
							status:0,
							message: err
						});
					} else {
						if(notifica.message != null)
							response.push(notifica);

						if(index === arr.length-1){	
							res.json({
								status: 1,
								body: response
							});
						}
					}
				})
			});			
		});
	})
	.catch(function(err) {
		console.log("ERR: "+err);
        res.json({
        	status : 0,
        	message: err
        });
    });
});

apiRoutes.post('/cultivations/near', function(req, res){
	print("POST /api/cultivations/near");
	var idUtente = req.id;
	var distance = req.body.distance; //distance in km
	var lat = req.body.lat;
	var tipo = req.body.tipo;
	var lng = req.body.long;
	
	var query = "CALL getNearCultivations("+lat+", "+lng+", "+distance+", "+idUtente+", '"+tipo+"');";
	connection.acquire(function(err, con) {
		con.query(query, function(err, result) {
			con.release();
		    if (err) {
		        console.log("ERRORE"+JSON.stringify(err));
		        res.send({status: 1, message: err});
		    } else if( result[0].length < 1 ) {
		        res.send({status: 2, message: 'No cultivations available around you'})
		    } else {
		    	res.json({
		    		status: 3,
		    		body: result[0]
		    	});
		    }
		})
	});
});

function print(str){
	var d = new Date();
	console.log("["+d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()+" - "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"] "+str);
}

app.use('/api', apiRoutes);

var server = app.listen(port, server_address, function() {
    console.log('Server listening on http://'+server.address().address+":"+server.address().port);
});