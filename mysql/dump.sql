CREATE DATABASE myorto; 
USE myorto;
-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Nov 23, 2016 alle 11:59
-- Versione del server: 5.6.28
-- Versione PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `myorto`
--

DELIMITER $$
--
-- Procedure
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCons` (IN `p1` INT(10))  NO SQL
BEGIN
    SELECT * 
  FROM(
    SELECT idPianta, nome, cons
        FROM pianta, relazione
        WHERE idP1 = p1
        AND idP2 = pianta.idPianta
        UNION
        SELECT idPianta, nome, 0
        FROM pianta
        WHERE NOT EXISTS (
            SELECT *
            FROM relazione
            WHERE idP1 = p1
            AND idP2 = pianta.idPianta
        )
  ) as T
  ORDER BY T.cons DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getMultiCons` (IN `idp1` INT(10) UNSIGNED, IN `idp2` INT(10) UNSIGNED, IN `idp3` INT(10) UNSIGNED, IN `idp4` INT(10) UNSIGNED, IN `idp5` INT(10) UNSIGNED, IN `idp6` INT(10) UNSIGNED, IN `idp7` INT(10) UNSIGNED, IN `idp8` INT(10) UNSIGNED, IN `idp9` INT(10) UNSIGNED, IN `idp10` INT(10) UNSIGNED, IN `dim` INT(10) UNSIGNED)  NO SQL
BEGIN
  SET @query = CONCAT('SELECT T.idPianta, T.nome, SUM(cons) as cons FROM(');

  IF  NULLIF(idp1, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,'SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp1,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp1,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp2, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp2,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp2,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp3, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp3,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp3,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp4, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp4,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp4,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp5, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp5,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp5,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp6, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp6,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp6,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp7, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp7,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp7,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp8, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp8,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp8,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp9, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp9,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp9,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    IF NULLIF(idp10, '') IS NOT NULL THEN
      SET @query = CONCAT(@query,' UNION ALL SELECT pianta.idPianta, pianta.nome, cons FROM pianta , relazione WHERE idP1 = ',idp10,' AND idP2 = pianta.idPianta AND pianta.spaceSprout <= ',dim,' UNION ALL SELECT pianta.idPianta, pianta.nome, 0 as cons FROM pianta WHERE NOT EXISTS (SELECT * FROM relazione WHERE idP1 = ',idp10,' AND idP2 = pianta.idPianta) AND pianta.spaceSprout <= ',dim);
    END IF;
    
    SET @query = CONCAT(@query,') as T GROUP BY idPianta ORDER BY cons desc');
  
  PREPARE stmt FROM @query;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getNearCultivations` (IN `lat` FLOAT(10,6), IN `lng` FLOAT(10,6), IN `dis` INT, IN `id` INT(10), IN `tp` ENUM('v','o'))  NO SQL
SELECT id, idUtente, nome, tipo, dataInizio, ( 6371 * acos( cos( radians(lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(lng) ) + sin( radians(lat) ) * sin( radians( latitude ) ) ) ) AS distance FROM coltivazione HAVING distance < dis AND tipo = tp AND idUtente != id ORDER BY dataInizio desc LIMIT 0 , 20$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella `coltivazione`
--

CREATE TABLE `coltivazione` (
  `id` int(10) UNSIGNED NOT NULL,
  `idUtente` int(10) UNSIGNED NOT NULL,
  `Tipo` enum('v','o') NOT NULL,
  `nome` varchar(50) NOT NULL DEFAULT 'Il mio orto',
  `latitude` float(10,6) NOT NULL,
  `longitude` float(10,6) NOT NULL,
  `dataInizio` date NOT NULL,
  `dataFine` date NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `pianta`
--

CREATE TABLE `pianta` (
  `idPianta` int(10) UNSIGNED NOT NULL,
  `nome` varchar(30) NOT NULL,
  `temp_ger_min` decimal(10,0) NOT NULL COMMENT 'Temperatura minima germogliazione',
  `temp_ger_max` decimal(10,0) NOT NULL COMMENT 'Temperatura massima germogliazione',
  `temp_cre_min` decimal(10,0) NOT NULL COMMENT 'Temperatura minima crescina',
  `temp_cre_max` decimal(10,0) NOT NULL COMMENT 'Temperatura massima crescita',
  `time_ger` smallint(6) NOT NULL COMMENT 'Giorni per germogliazione',
  `time_cre` smallint(6) NOT NULL COMMENT 'Giorni per crescita',
  `spaceSeed` smallint(6) DEFAULT NULL COMMENT 'Spazio seme',
  `spaceSprout` smallint(6) DEFAULT NULL COMMENT 'Spazio impianto'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `pianta`
--

INSERT INTO `pianta` (`idPianta`, `nome`, `temp_ger_min`, `temp_ger_max`, `temp_cre_min`, `temp_cre_max`, `time_ger`, `time_cre`, `spaceSeed`, `spaceSprout`) VALUES
(1, 'aglio', '0', '0', '5', '15', 0, 0, NULL, 13),
(2, 'pomodoro', '0', '0', '12', '25', 0, 0, NULL, 38),
(3, 'basilico', '0', '0', '14', '28', 0, 0, NULL, 28),
(4, 'carciofo', '0', '0', '8', '22', 0, 0, NULL, 90),
(5, 'bietola da coste', '0', '0', '5', '30', 0, 0, NULL, 30),
(7, 'cavolo bruxelles', '0', '0', '7', '23', 0, 0, NULL, 40),
(8, 'cavolo broccolo', '0', '0', '16', '24', 0, 0, NULL, 40),
(9, 'cavolo cappuccio', '0', '0', '7', '18', 0, 0, NULL, 40),
(10, 'cavolfiore', '0', '0', '8', '24', 0, 0, NULL, 40),
(11, 'cavolo verza', '0', '0', '6', '22', 0, 0, NULL, 50),
(12, 'cavolo rapa', '0', '0', '5', '22', 0, 0, NULL, 30),
(13, 'cetriolo', '0', '0', '15', '30', 0, 0, NULL, 60),
(15, 'cicoria', '0', '0', '6', '22', 0, 0, NULL, 30),
(16, 'cipolla', '0', '0', '5', '35', 0, 0, NULL, 15),
(18, 'finocchio', '0', '0', '7', '30', 0, 0, NULL, 30),
(19, 'lattuga', '0', '0', '5', '24', 0, 0, NULL, 25),
(20, 'melanzana', '0', '0', '15', '35', 0, 0, NULL, 50),
(22, 'peperone', '0', '0', '15', '35', 0, 0, NULL, 50),
(23, 'porro', '0', '0', '5', '30', 0, 0, NULL, 10),
(24, 'rapa', '0', '0', '5', '27', 0, 0, NULL, 20),
(26, 'zucca', '0', '0', '5', '41', 0, 0, NULL, 100),
(27, 'zucchina', '0', '0', '5', '41', 0, 0, NULL, 80),
(28, 'carota', '0', '0', '6', '35', 0, 0, NULL, 10),
(29, 'ravanello', '0', '0', '6', '35', 0, 0, 10, 15),
(31, 'indivia', '0', '0', '5', '22', 0, 0, 30, 30),
(33, 'fagiolo', '0', '0', '15', '35', 0, 0, 5, 15);

-- --------------------------------------------------------

--
-- Struttura della tabella `piantecoltivazione`
--

CREATE TABLE `piantecoltivazione` (
  `id` int(10) UNSIGNED NOT NULL,
  `idColtivazione` int(10) UNSIGNED NOT NULL,
  `idPianta` int(10) UNSIGNED NOT NULL,
  `message` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `relazione`
--

CREATE TABLE `relazione` (
  `idP1` int(10) UNSIGNED NOT NULL,
  `idP2` int(10) UNSIGNED NOT NULL,
  `cons` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `relazione`
--

INSERT INTO `relazione` (`idP1`, `idP2`, `cons`) VALUES
(1, 2, 1),
(1, 7, -1),
(1, 8, -1),
(1, 9, -1),
(1, 10, -1),
(1, 11, -1),
(1, 12, -1),
(1, 13, 1),
(1, 19, 1),
(1, 27, 1),
(1, 28, 1),
(1, 33, -1),
(2, 1, 1),
(2, 2, 1),
(2, 3, 1),
(2, 7, 1),
(2, 8, 1),
(2, 9, 1),
(2, 10, 1),
(2, 11, 1),
(2, 12, 1),
(2, 13, -1),
(2, 15, 1),
(2, 16, 1),
(2, 18, -1),
(2, 19, 1),
(2, 23, 1),
(2, 28, 1),
(2, 29, 1),
(2, 33, -1),
(3, 2, 1),
(4, 16, 1),
(4, 19, 1),
(4, 22, 1),
(4, 23, 1),
(4, 24, 1),
(5, 7, 1),
(5, 8, 1),
(5, 9, 1),
(5, 10, 1),
(5, 11, 1),
(5, 12, 1),
(5, 28, 1),
(5, 29, 1),
(5, 33, 1),
(7, 1, -1),
(7, 2, 1),
(7, 5, 1),
(7, 13, 1),
(7, 16, -1),
(7, 19, 1),
(7, 22, 1),
(7, 23, 1),
(7, 29, 1),
(7, 33, 1),
(8, 1, -1),
(8, 2, 1),
(8, 5, 1),
(8, 13, 1),
(8, 16, -1),
(8, 19, 1),
(8, 22, 1),
(8, 23, 1),
(8, 29, 1),
(8, 33, 1),
(9, 1, -1),
(9, 2, 1),
(9, 5, 1),
(9, 13, 1),
(9, 16, -1),
(9, 19, 1),
(9, 22, 1),
(9, 23, 1),
(9, 29, 1),
(9, 33, 1),
(10, 1, -1),
(10, 2, 1),
(10, 5, 1),
(10, 13, 1),
(10, 16, -1),
(10, 19, 1),
(10, 22, 1),
(10, 23, 1),
(10, 29, 1),
(10, 33, 1),
(11, 1, -1),
(11, 2, 1),
(11, 5, 1),
(11, 13, 1),
(11, 16, -1),
(11, 19, 1),
(11, 22, 1),
(11, 23, 1),
(11, 29, 1),
(11, 33, 1),
(12, 1, -1),
(12, 2, 1),
(12, 5, 1),
(12, 13, 1),
(12, 16, -1),
(12, 19, 1),
(12, 22, 1),
(12, 23, 1),
(12, 29, 1),
(12, 33, 1),
(13, 1, 1),
(13, 2, -1),
(13, 7, 1),
(13, 8, 1),
(13, 9, 1),
(13, 10, 1),
(13, 11, 1),
(13, 12, 1),
(13, 16, 1),
(13, 18, 1),
(13, 19, 1),
(13, 23, 1),
(13, 33, 1),
(15, 2, 1),
(15, 18, 1),
(15, 19, 1),
(15, 28, 1),
(15, 33, 1),
(16, 1, -1),
(16, 2, 1),
(16, 13, 1),
(16, 16, -1),
(16, 18, -1),
(16, 19, 1),
(16, 23, 1),
(16, 27, 1),
(16, 28, 1),
(18, 2, -1),
(18, 13, 1),
(18, 15, 1),
(18, 19, 1),
(18, 22, 1),
(18, 23, 1),
(18, 33, -1),
(19, 2, 1),
(19, 7, 1),
(19, 8, 1),
(19, 9, 1),
(19, 10, 1),
(19, 11, 1),
(19, 12, 1),
(19, 13, 1),
(19, 15, 1),
(19, 16, 1),
(19, 18, 1),
(19, 20, 1),
(19, 22, 1),
(19, 23, 1),
(19, 28, 1),
(19, 29, 1),
(19, 33, 1),
(20, 22, -1),
(20, 33, 1),
(22, 7, 1),
(22, 8, -1),
(22, 9, 1),
(22, 10, 1),
(22, 11, 1),
(22, 12, 1),
(22, 18, 1),
(22, 19, 1),
(22, 20, -1),
(22, 28, 1),
(23, 2, 1),
(23, 7, 1),
(23, 8, 1),
(23, 9, 1),
(23, 10, 1),
(23, 11, 1),
(23, 12, 1),
(23, 13, 1),
(23, 16, 1),
(23, 18, 1),
(23, 19, 1),
(23, 22, 1),
(23, 28, 1),
(23, 33, -1),
(24, 1, 1),
(24, 13, 1),
(24, 16, 1),
(24, 19, 1),
(24, 23, -1),
(24, 27, 1),
(24, 29, 1),
(24, 33, 1),
(26, 13, -1),
(27, 16, 1),
(27, 19, 1),
(27, 33, 1),
(28, 1, 1),
(28, 2, 1),
(28, 5, 1),
(28, 13, 1),
(28, 15, 1),
(28, 16, 1),
(28, 17, 1),
(28, 19, 1),
(28, 22, 1),
(28, 23, 1),
(28, 27, 1),
(28, 29, 1),
(28, 33, 1),
(29, 2, 1),
(29, 5, 1),
(29, 7, 1),
(29, 8, 1),
(29, 9, 1),
(29, 10, 1),
(29, 11, 1),
(29, 12, 1),
(29, 19, 1),
(29, 23, 1),
(29, 28, 1),
(29, 33, 1),
(31, 2, 1),
(33, 1, -1),
(33, 2, -1),
(33, 5, 1),
(33, 7, 1),
(33, 8, 1),
(33, 9, 1),
(33, 10, 1),
(33, 11, 1),
(33, 12, 1),
(33, 13, 1),
(33, 15, 1),
(33, 16, -1),
(33, 18, -1),
(33, 19, 1),
(33, 20, 1),
(33, 23, -1),
(33, 27, 1),
(33, 28, 1),
(33, 29, 1),
(33, 33, -1);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `strongrelations`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `strongrelations` (
`idP1` int(10) unsigned
,`idP2` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `idUtente` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `weakrelations`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `weakrelations` (
`idP1` int(10) unsigned
,`idP2` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Struttura per la vista `strongrelations`
--
DROP TABLE IF EXISTS `strongrelations`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `strongrelations`  AS  select `T`.`idP1` AS `idP1`,`T`.`idP2` AS `idP2` from `relazione` `T` where exists(select 1 from `relazione` where ((`relazione`.`idP2` = `T`.`idP1`) and (`relazione`.`idP1` = `T`.`idP2`))) group by `T`.`idP1`,`T`.`idP2` ;

-- --------------------------------------------------------

--
-- Struttura per la vista `weakrelations`
--
DROP TABLE IF EXISTS `weakrelations`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `weakrelations`  AS  select `T`.`idP1` AS `idP1`,`T`.`idP2` AS `idP2` from `relazione` `T` where (not(exists(select 1 from `relazione` where ((`relazione`.`idP2` = `T`.`idP1`) and (`relazione`.`idP1` = `T`.`idP2`))))) group by `T`.`idP1`,`T`.`idP2` ;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `coltivazione`
--
ALTER TABLE `coltivazione`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUtente` (`idUtente`);

--
-- Indici per le tabelle `pianta`
--
ALTER TABLE `pianta`
  ADD PRIMARY KEY (`idPianta`);

--
-- Indici per le tabelle `piantecoltivazione`
--
ALTER TABLE `piantecoltivazione`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idColtivazione` (`idColtivazione`),
  ADD KEY `idPianta` (`idPianta`);

--
-- Indici per le tabelle `relazione`
--
ALTER TABLE `relazione`
  ADD UNIQUE KEY `idP1_2` (`idP1`,`idP2`),
  ADD KEY `idP1` (`idP1`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`idUtente`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `coltivazione`
--
ALTER TABLE `coltivazione`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `pianta`
--
ALTER TABLE `pianta`
  MODIFY `idPianta` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT per la tabella `piantecoltivazione`
--
ALTER TABLE `piantecoltivazione`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `idUtente` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `coltivazione`
--
ALTER TABLE `coltivazione`
  ADD CONSTRAINT `coltivazione_ibfk_1` FOREIGN KEY (`idUtente`) REFERENCES `Utente` (`idUtente`);

--
-- Limiti per la tabella `piantecoltivazione`
--
ALTER TABLE `piantecoltivazione`
  ADD CONSTRAINT `piantecoltivazione_ibfk_1` FOREIGN KEY (`idColtivazione`) REFERENCES `coltivazione` (`id`),
  ADD CONSTRAINT `piantecoltivazione_ibfk_2` FOREIGN KEY (`idPianta`) REFERENCES `Pianta` (`idPianta`);

--
-- Limiti per la tabella `relazione`
--
ALTER TABLE `relazione`
  ADD CONSTRAINT `relazione_ibfk_1` FOREIGN KEY (`idP1`) REFERENCES `Pianta` (`idPianta`) ON UPDATE CASCADE;
