var mysql = require('mysql');
var config = require('./config');
 
function DbConnection() {
    this.pool = null;

    this.init = function() {
        this.pool = mysql.createPool({
            connectionLimit: 100,
            host: config.url,
            port: config.db_port,
            user: config.db_user,
            password: config.db_password,
            database: config.db_name
        });
    };

    this.acquire = function(callback) {
        this.pool.getConnection(function(err, connection) {
            callback(err, connection);
        });
    };
}
 
module.exports = new DbConnection();